//
//  main.m
//  TradeSimulator
//
//  Created by Guillermo Orellana on 05/02/14.
//  Copyright (c) 2014 Credit Suisse. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TAAppDelegate class]));
    }
}
